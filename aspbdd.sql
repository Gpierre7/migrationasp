-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 19 juil. 2021 à 11:55
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `aspbdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'Matériel en location ou en vente', NULL, NULL),
(2, 'Consommables', NULL, NULL),
(3, 'Incontinence', NULL, NULL),
(4, 'Les rampes d\'accès', NULL, NULL),
(5, 'Le Stannah', NULL, NULL),
(6, 'Les objets connectés', NULL, NULL),
(7, 'Couches et culottes', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_16_054100_create_produits_table', 1),
(5, '2021_07_16_064427_create_categories_table', 1),
(6, '2021_07_16_064634_create_sous_categories_table', 1),
(7, '2021_07_16_065024_create_types_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_sous_categorie` bigint(20) UNSIGNED NOT NULL,
  `id_type` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `reference`, `description`, `image`, `lien`, `document`, `marque`, `created_at`, `updated_at`, `id_sous_categorie`, `id_type`) VALUES
(1, ' Fauteuil LIBERTY e avec 4 roues freinées', '', '- Inclinaison 100% : Assise / Dossier / Repose jambes synchronisés.\r\n                - Modules de couette remplaçables et lavables en machine à 30°\r\n                - Couette garnie de fibre creuse siliconée pour un allégement des pressions.\r\n                - Fonction massage par vibration du coussin lombaire.\r\n                - 3 types revêtements et plusieurs coloris disponibles : PVC (Cacao, Ebène, Framboise), Velours déperlant (Choco Edition, Black Edition), Tissu chiné (Gris ou Marron).\r\n                - Normes Non Feu M2.\r\n                - 5 Largeurs d’assise : 30 cm, 36 cm, 42 cm, 48 cm et 54 cm\r\n                - 5 Options disponibles :\r\n                - Tablette latérale- Cale tronc ajustable (droit/gauche) – Appareil de soutien partiel de la tête – Coussin Classe II – Kit Batterie\r\n                - Fabrication Française', '', '', '', '', NULL, NULL, 5, 1),
(2, 'Fauteuil confort Coquille Prémium', '', '- GIR 1 à GIR 4 avec entente préalable\r\n                - Fabrication française, inclinaison 100% électrique, télécommande verrouillable.\r\n                - Assise garnie d’une mousse à mémoire de forme classe 2,3 types de revêtements, plusieurs coloris.\r\n                - 15 tailles assises possibles (5 largeurs et 3 profondeurs)\r\n                - Module de couettes remplaçables, amovibles, lavables en machine dés 30°,\r\n                - Garniture fibres siliconées pour un allègement des pressions,\r\n                - Version EPHAD non électrique, tg flo grosses roues.\r\n                - Options: latérale et cale tronc ajustable.', '', '', '', '', NULL, NULL, 5, 1),
(3, ' Fauteuil confort Cocoon', '', 'Fauteuil releveur\r\n                - Fabrication française,\r\n                - confort d’utilisation: 1 et 2 moteurs (commande séparément buste et jambes pour le 2 moteurs).\r\n                - Dossier ajustable et bascule d’assise avec coussin repose jambes inclinables,\r\n                - accoudoirs escamotables (facilité transfert)\r\n                - Fonction verticalisateur.\r\n                - Adaptable à la morphologie: plusieurs largeurs possibles (2) et 2 choix de profondeurs, existe en version XXL (220kg),\r\n                - choix couleurs et revêtements, couette déhoussable, lavable en machine 30°.\r\n                - Options: tablette, porte revue', '', '', '', '', NULL, NULL, 5, 1),
(4, ' Fauteuil confort Weely’nov', '', '- Réglages multiples adaptés à la morphologie et pathologie de l’utilisateur,\r\n                - Excellent positionnement pour utilisateurs sujets aux positionnements prolongés.\r\n                - Accoudoirs mobiles,\r\n                - Réglage repose jambes et appui tête.\r\n                - 6 roues, maniabilité et grande stabilité, système franchissement de seuil d’obstacles jusqu’à 13cm.\r\n                - Dossier et coussin couette amovible, lavable en machine à 30°, plusieurs coloris et revêtements non feu.\r\n                - Poids maxi 140kg.\r\n                - Options: tablette latérale et ventrale cale tronc, coussin assise classe 2, surpalettes pour mollets', '', '', '', '', NULL, NULL, 5, 1),
(5, 'Fauteuil roulant manuel', '', '- léger et multi réglable, accoudoirs amovibles et relevables, dossier inclinable et capitonné, démontage rapide\r\n                ', '', '', '', '', NULL, NULL, 4, 1),
(6, 'Fauteuil de transfert', '', '- Fauteuil à pousser compact et pliable avec accoudoir relevable', '', '', '', '', NULL, NULL, 4, 1),
(7, 'Déambulateur Rollator', '', '- Cadre de marche réglable en hauteur, existe pliable\r\n                - autres modèles rollator existent en 2, 3 ou 4 roues. En fonction de modèles, avec assise, dossier, panier, freins pour usage intérieur ou extérieur', '', '', '', '', NULL, NULL, 4, 1),
(8, 'Canne tripode et autres modèles', '', '', '', '', '', '', NULL, NULL, 4, 1),
(9, 'Tire lait kitett', '', 'Malette pratique pour le transport et le rangement. Simple ou double pompage, plusieurs diamètres téterelles disponibles', 'public/img/tire_lait_kitett', '', '', '', NULL, NULL, 1, 2),
(10, 'Tire lait medela', '', 'Silencieux programme néo nat. Double pompages', 'public/img/tire_lait_symphonie.png', '', '', 'Medela', NULL, NULL, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `sous_categories`
--

CREATE TABLE `sous_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_categorie` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sous_categories`
--

INSERT INTO `sous_categories` (`id`, `libelle`, `created_at`, `updated_at`, `id_categorie`) VALUES
(1, 'Lits médicalisés', NULL, NULL, 1),
(2, 'Transfert et mobilité', NULL, NULL, 1),
(3, 'Matelas et coussins prévention d’escarre', NULL, NULL, 1),
(4, 'Mobilité et aide à la marche', NULL, NULL, 1),
(5, 'Les fauteuils confort', NULL, NULL, 1),
(6, 'Salle de bain / Toilettes', NULL, NULL, 1),
(7, 'Ustensile cuisine / Repas', NULL, NULL, 1),
(8, 'Consommables', NULL, NULL, 2),
(9, 'Incontinence', NULL, NULL, 3),
(10, 'Les rampes d\'accès', NULL, NULL, 4),
(11, 'Le Stannah', NULL, NULL, 5),
(12, 'Pilulier connecté', NULL, NULL, 6),
(13, 'Téléassistance connectée', NULL, NULL, 6),
(14, 'Tensiométre', NULL, NULL, 6),
(15, 'Oxymétre', NULL, NULL, 6),
(16, 'Téléphone et boitier', NULL, NULL, 6),
(17, 'Caméra de surveillance', NULL, NULL, 6),
(18, 'Couches et culottes', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'Nos produits', NULL, NULL),
(2, 'Maternité', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produits_id_sous_categorie_foreign` (`id_sous_categorie`),
  ADD KEY `produits_id_type_foreign` (`id_type`);

--
-- Index pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sous_categories_id_categorie_foreign` (`id_categorie`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_id_sous_categorie_foreign` FOREIGN KEY (`id_sous_categorie`) REFERENCES `sous_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `produits_id_type_foreign` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD CONSTRAINT `sous_categories_id_categorie_foreign` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
