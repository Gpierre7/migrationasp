<?php

namespace App\Models;

use App\Models\{Categorie, Produit};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sous_categorie extends Model
{
    use HasFactory;
    protected $fillable = ['libelle', 'id_categorie'];
    public function categorie()
    {
        return $this->hasOne(Categorie::class);
    }
    public function produit()
    {
        return $this->hasMany(Produit::class);
    }    
}
