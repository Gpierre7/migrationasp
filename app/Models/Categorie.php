<?php

namespace App\Models;

use App\Models\Sous_categorie;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $fillable = ['libelle'];
    public function Sous_categorie()
    {
        return $this->hasMany(Sous_categorie::class);
    }

}
