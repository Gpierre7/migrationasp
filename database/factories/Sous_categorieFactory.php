<?php

namespace Database\Factories;

use App\Models\Categorie;
use App\Models\Sous_categorie;
use Illuminate\Database\Eloquent\Factories\Factory;

class Sous_categorieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sous_categorie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word(),
            'id_categorie' => Categorie::factory(),
        ];
    }
}
