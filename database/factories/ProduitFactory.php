<?php

namespace Database\Factories;

use App\Models\Marque;
use App\Models\Produit;
use App\Models\Sous_categorie;
use App\Models\Type;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProduitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Produit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->word(),
            'reference' => $this->faker->unique()->Str::random(10),
            'description' => $this->faker->paragraph(),
            'image' => $this->faker->url,
            'lien' => $this->faker->url,
            'document' => $this->faker->url,
            'marque' => $this->faker->word(),
            'id_sous_categorie' => Sous_categorie::factory(),
            'id_type' => Type::factory(),
        ];
    }
}
