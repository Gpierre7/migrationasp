<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produits')->insert([

            ////////////////////////////////////////////////////
            ////////////////// ACCUEIL /////////////////////////
            ////////////////////////////////////////////////////
            [
                'id' => 1,
                'nom' => ' Fauteuil LIBERTY e avec 4 roues freinées',
                'reference' => '',
                'description' => '- Inclinaison 100% : Assise / Dossier / Repose jambes synchronisés.
                - Modules de couette remplaçables et lavables en machine à 30°
                - Couette garnie de fibre creuse siliconée pour un allégement des pressions.
                - Fonction massage par vibration du coussin lombaire.
                - 3 types revêtements et plusieurs coloris disponibles : PVC (Cacao, Ebène, Framboise), Velours déperlant (Choco Edition, Black Edition), Tissu chiné (Gris ou Marron).
                - Normes Non Feu M2.
                - 5 Largeurs d’assise : 30 cm, 36 cm, 42 cm, 48 cm et 54 cm
                - 5 Options disponibles :
                - Tablette latérale- Cale tronc ajustable (droit/gauche) – Appareil de soutien partiel de la tête – Coussin Classe II – Kit Batterie
                - Fabrication Française',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '5',
                'id_type' => '1',
            ],
            [
                'id' => 2,
                'nom' => 'Fauteuil confort Coquille Prémium',
                'reference' => '',
                'description' => '- GIR 1 à GIR 4 avec entente préalable
                - Fabrication française, inclinaison 100% électrique, télécommande verrouillable.
                - Assise garnie d’une mousse à mémoire de forme classe 2,3 types de revêtements, plusieurs coloris.
                - 15 tailles assises possibles (5 largeurs et 3 profondeurs)
                - Module de couettes remplaçables, amovibles, lavables en machine dés 30°,
                - Garniture fibres siliconées pour un allègement des pressions,
                - Version EPHAD non électrique, tg flo grosses roues.
                - Options: latérale et cale tronc ajustable.',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '5',
                'id_type' => '1',
            ],
            [
                'id' => 3,
                'nom' => ' Fauteuil confort Cocoon',
                'reference' => '',
                'description' => 'Fauteuil releveur
                - Fabrication française,
                - confort d’utilisation: 1 et 2 moteurs (commande séparément buste et jambes pour le 2 moteurs).
                - Dossier ajustable et bascule d’assise avec coussin repose jambes inclinables,
                - accoudoirs escamotables (facilité transfert)
                - Fonction verticalisateur.
                - Adaptable à la morphologie: plusieurs largeurs possibles (2) et 2 choix de profondeurs, existe en version XXL (220kg),
                - choix couleurs et revêtements, couette déhoussable, lavable en machine 30°.
                - Options: tablette, porte revue',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '5',
                'id_type' => '1',
            ],
            [
                'id' => 4,
                'nom' => ' Fauteuil confort Weely’nov',
                'reference' => '',
                'description' => '- Réglages multiples adaptés à la morphologie et pathologie de l’utilisateur,
                - Excellent positionnement pour utilisateurs sujets aux positionnements prolongés.
                - Accoudoirs mobiles,
                - Réglage repose jambes et appui tête.
                - 6 roues, maniabilité et grande stabilité, système franchissement de seuil d’obstacles jusqu’à 13cm.
                - Dossier et coussin couette amovible, lavable en machine à 30°, plusieurs coloris et revêtements non feu.
                - Poids maxi 140kg.
                - Options: tablette latérale et ventrale cale tronc, coussin assise classe 2, surpalettes pour mollets',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '5',
                'id_type' => '1',
            ],
            [
                'id' => 5,
                'nom' => 'Fauteuil roulant manuel',
                'reference' => '',
                'description' => '- léger et multi réglable, accoudoirs amovibles et relevables, dossier inclinable et capitonné, démontage rapide
                ',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '4',
                'id_type' => '1',
            ],
            [
                'id' => 6,
                'nom' => 'Fauteuil de transfert',
                'reference' => '',
                'description' => '- Fauteuil à pousser compact et pliable avec accoudoir relevable',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '4',
                'id_type' => '1',
            ],
            [
                'id' => 7,
                'nom' => 'Déambulateur Rollator',
                'reference' => '',
                'description' => '- Cadre de marche réglable en hauteur, existe pliable
                - autres modèles rollator existent en 2, 3 ou 4 roues. En fonction de modèles, avec assise, dossier, panier, freins pour usage intérieur ou extérieur',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '4',
                'id_type' => '1',
            ],
            [
                'id' => 8,
                'nom' => 'Canne tripode et autres modèles',
                'reference' => '',
                'description' => '',
                'image' => '',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '4',
                'id_type' => '1',
            ],
            [
                'id' => 9,
                'nom' => 'Tire lait kitett',
                'reference' => '',
                'description' => 'Malette pratique pour le transport et le rangement. Simple ou double pompage, plusieurs diamètres téterelles disponibles',
                'image' => 'public/img/tire_lait_kitett',
                'lien' => '',
                'document' => '',
                'marque' => '',
                'id_sous_categorie' => '1',
                'id_type' => '2',
            ],
            [
                'id' => 10,
                'nom' => 'Tire lait medela',
                'reference' => '',
                'description' => 'Silencieux programme néo nat. Double pompages',
                'image' => 'public/img/tire_lait_symphonie.png',
                'lien' => '',
                'document' => '',
                'marque' => 'Medela',
                'id_sous_categorie' => '1',
                'id_type' => '2',
            ],

        ]);
    }
}
