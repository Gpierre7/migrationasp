<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\TypeSeeder;
use Database\Seeders\ProduitsSeeder;
use Database\Seeders\CategoriesSeeder;
use Database\Seeders\SousCategoriesSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(SousCategoriesSeeder::class);
        $this->call(ProduitsSeeder::class);
    }
}
