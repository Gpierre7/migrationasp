<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([

            ////////////////////////////////////////////////////
            ////////////////// ACCUEIL /////////////////////////
            ////////////////////////////////////////////////////
            [
                'id' => 1,
                'libelle' => 'Matériel en location ou en vente',
            ],
            [
                'id' => 2,
                'libelle' => 'Consommables',
            ],
            [
                'id' => 3,
                'libelle' => 'Incontinence',
            ],
            [
                'id' => 4,
                'libelle' => "Les rampes d'accès",
            ],
            [
                'id' => 5,
                'libelle' => 'Le Stannah',
            ],
            [
                'id' => 6,
                'libelle' => 'Les objets connectés',
            ],
            [
                'id' => 7,
                'libelle' => 'Couches et culottes',
            ]
        ]);
    }
}
