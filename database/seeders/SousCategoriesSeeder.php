<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SousCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sous_categories')->insert([

            ////////////////////////////////////////////////////
            ////////////////// ACCUEIL /////////////////////////
            ////////////////////////////////////////////////////
            [
                'id' => 1,
                'libelle' => 'Lits médicalisés',
                'id_categorie' => '1'
            ],
            [
                'id' => 2,
                'libelle' => 'Transfert et mobilité',
                'id_categorie' => '1'
            ],
            [
                'id' => 3,
                'libelle' => 'Matelas et coussins prévention d’escarre',
                'id_categorie' => '1'
            ],
            [
                'id' => 4,
                'libelle' => 'Mobilité et aide à la marche',
                'id_categorie' => '1'
            ],
            [
                'id' => 5,
                'libelle' => 'Les fauteuils confort',
                'id_categorie' => '1'
            ],
            [
                'id' => 6,
                'libelle' => 'Salle de bain / Toilettes',
                'id_categorie' => '1'
            ],
            [
                'id' => 7,
                'libelle' => 'Ustensile cuisine / Repas',
                'id_categorie' => '1'
            ],
            [
                'id' => 8,
                'libelle' => 'Consommables',
                'id_categorie' => '2'
            ],
            [
                'id' => 9,
                'libelle' => 'Incontinence',
                'id_categorie' => '3'
            ],
            [
                'id' => 10,
                'libelle' => "Les rampes d'accès",
                'id_categorie' => '4'
            ],
            [
                'id' => 11,
                'libelle' => 'Le Stannah',
                'id_categorie' => '5'
            ],
            [
                'id' => 12,
                'libelle' => 'Pilulier connecté',
                'id_categorie' => '6'
            ],
            [
                'id' => 13,
                'libelle' => 'Téléassistance connectée',
                'id_categorie' => '6'
            ],
            [
                'id' => 14,
                'libelle' => 'Tensiométre',
                'id_categorie' => '6'
            ],
            [
                'id' => 15,
                'libelle' => 'Oxymétre',
                'id_categorie' => '6'
            ],
            [
                'id' => 16,
                'libelle' => 'Téléphone et boitier',
                'id_categorie' => '6'
            ],
            [
                'id' => 17,
                'libelle' => 'Caméra de surveillance',
                'id_categorie' => '6'
            ],
           
            [
                'id' => 18,
                'libelle' => 'Couches et culottes',
                'id_categorie' => '7'
            ],
           
           
        ]);
    }
}
