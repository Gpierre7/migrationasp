<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('produits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('reference');
            $table->text('description');
            $table->string('image');
            $table->string('lien');
            $table->string('document');
            $table->string('marque');
            $table->timestamps();
            $table->unsignedBigInteger('id_sous_categorie');
            $table->foreign('id_sous_categorie')
                ->references('id')
                ->on('sous_categories')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->unsignedBigInteger('id_type');
            $table->foreign('id_type')
                ->references('id')
                ->on('types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
